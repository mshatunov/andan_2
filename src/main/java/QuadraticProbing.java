import java.util.Objects;
import java.util.stream.Stream;

/* Quadratic Probing */
	public class QuadraticProbing<AnyType>
	{
		private static final int DEFAULT_TABLE_SIZE = 13;
		private HashEntry<AnyType> [ ] array; // The array of elements
		public int currentSize;
		public int currentCapacity;
	
	public static class HashEntry<AnyType>
	{
		/* Initialize the entries here. You can write a constructor for the same */
		public AnyType  element; 
		public boolean isActive;  // For Lazy deletion
		public String toString()
		{
			if(this.element!=null)
				return (String) element;
			else
				return "NULL";
		}

		public HashEntry(AnyType element, boolean isActive) {
			this.element = element;
			this.isActive = isActive;
		}

	}


/* Construct the hash table */
	public QuadraticProbing( )
	{
		this( DEFAULT_TABLE_SIZE );
	}

/* Construct the hash table */

	public QuadraticProbing( int size )
	{
		/* allocate memory to hash table */
		array = new HashEntry[size];
		currentCapacity = size;
	}


/* Return true if currentPos exists and is active - Lazy Deletion*/
	public boolean isActive(int position)
	{
		return array[position] != null && array[position].isActive;
	}
	

/* Find an item in the hash table. */
	public boolean contains( AnyType x )
	{
		/* Should return the active status of key in hash table */
		return isActive(getPosition(x));
	}


/* Insert into the Hash Table */
	
	public void insert( AnyType x )
	{
		/* Insert an element */
		int position = getPosition(x);
		if (!isActive(position)) {
			array[position] = new HashEntry<>(x, true);
			currentSize++;
		}
        if (currentSize > currentCapacity * 0.3) {
            rehash();
        }
	}


/* Remove from the hash table. */
	
	public void remove( AnyType x )	
	{
		/* Lazy Deletion*/
		int position = getPosition(x);
		if (isActive(position)) {
			array[position].isActive = false;
			currentSize--;
		}
   	}

   
/* Rehashing for quadratic probing hash table */
	private void rehash( )
	{
		int newCapacity = getNextPrime(currentCapacity*2);
		QuadraticProbing<AnyType> newHashTable = new QuadraticProbing<>(newCapacity);
		currentCapacity = newCapacity;
		Stream.of(array)
				.filter(Objects::nonNull)
				.filter(p -> p.isActive)
				.forEach(p -> newHashTable.insert(p.element));
		array = newHashTable.array;
	}

        private int getNextPrime(int integer) {
            if (integer % 2 == 0) {
                integer++;
            }
            while (!isPrime(integer)) {
                integer = integer + 2;
            }
            return integer;
        }

        private boolean isPrime(int integer) {
            boolean result;
            boolean done = false;

            if ((integer == 1) || (integer % 2 == 0)) {
                result = false;
            } else if ((integer == 2) || (integer == 3)) {
                result = true;
            } else {
                assert (integer % 2 != 0) && (integer >= 5);
                result = true;
                for (int divisor = 3; !done && (divisor * divisor <= integer); divisor = divisor + 2) {
                    if (integer % divisor == 0) {
                        result = false;
                        done = true;
                    }
                }
            }
            return result;
        }

        /* Hash Function */
	public int hash( String key, int tableSize, int offset )
	{
		/**  Make sure to type cast "AnyType"  to string 
		before calling this method - ex: if "x" is of "AnyType", 
		you should invoke this function as hash((x.toString()), tableSize) */

		int hash = key.hashCode();
        hash = (hash + offset * offset) % tableSize;
		if (hash < 0) {
			hash += tableSize;
		}
		return hash;

	}

	public int probe(AnyType x)
	{
		int probe = 0;
		int position = hash(x.toString(), currentCapacity, 0);
		int offset = 0;
		while (needToReProbe(position, x)) {
			offset++;
			position = hash(x.toString(), currentCapacity, offset);
			probe++;
		}
		return probe;
	}

	private int getPosition(AnyType x) {
		int position = hash(x.toString(), currentCapacity, 0);
		int offset = 0;
		while (needToReProbe(position, x)) {
			offset++;
			position = hash(x.toString(), currentCapacity, offset);
		}
		return position;
	}

	private boolean needToReProbe(int position, AnyType x) {
		return array[position] != null && !array[position].element.equals(x);
	}

}

