import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class Pair<K,V>
{
    /*
    The Pair class is intended to store key, value pairs. It'll be helpful
    for part 1.2 of the assignment.
    */
    public K key;
    public V value;

    public Pair(K key, V value)
   {
        this.key = key;
        this.value = value;
   }
}

/**************PART 1.2.1*******************/

public class HashTableChaining<K,V>
{
   /*
    Write your code for the hashtable with chaining here. You are allowed
    to use arraylists and linked lists.
    */
    private ArrayList<LinkedList<Pair<String, Integer>>> hashTable;

    private int currentSize;
    private int currentCapacity;

    public HashTableChaining(int capacity)
    {
        /*
        Initialize your hashtable with capacity equal to the input capacity.
        */
        hashTable = new ArrayList(capacity);
        IntStream.range(0, capacity)
                .forEach(i -> hashTable.add(i, new LinkedList<>()));
        currentCapacity = capacity;
    }
    public void insert(String key, int val)
    {
        /*
        Insert the key into the hashtable if it is not already in the hashtable.
        */
        if (currentSize == currentCapacity) {
            rehash();
        }
        int hash = hash(key);
        hashTable.get(hash)
                .add(new Pair<>(key, val));
        currentSize++;

    }
    public void remove(String key)
    {
        /*
        Remove key from the hashtable if it is present in the hashtable.
        */
        int hash = hash(key);
        Iterator<Pair<String, Integer>> iterator = hashTable.get(hash).iterator();
        while (iterator.hasNext()) {
            Pair<String, Integer> p = iterator.next();
            if (p.key.equals(key)) {
                iterator.remove();
                currentSize--;
            }
        }

    }
    public boolean contains(String key) {
        /* 
        Search the hashtable for key, if found return true else
        return false
        */
        int hash = hash(key);
        return hashTable.get(hash).stream()
                .anyMatch(p -> p.key.equals(key));
    }

    public int size()
    {
        /*
        return the total number of keys in the hashtable.
        */
        return currentSize;
    }

    public int hash(String key)
    {
        /*
        Use Horner's rule to compute the hashval and return it.
        */
        int hash = 0;
        for (int i = 0; i < key.length(); i++){
            hash = (37 * hash + key.charAt(i)) % currentCapacity;
        }
        return hash % currentCapacity;
    }

    public int getVal(String key)
    {
        
        /*
        return the value corresponding to the key in the hashtable.
        */
        int hash = hash(key);
        return hashTable.get(hash).stream()
                .filter(p -> p.key.equals(key))
                .map(p -> p.value)
                .findFirst()
                .orElse(0);
    }


    public void rehash()
    {
        /*
        Resize the hashtable such that the new size is the first prime number
        greater than two times the current size.
        For example, if current size is 5, then the new size would be 11.
        */
        int newCapacity = currentCapacity * 2 + 1;
        HashTableChaining newHashTable = new HashTableChaining(newCapacity);
        hashTable.stream()
                .flatMap(Collection::stream)
                .forEach(p -> newHashTable.insert(p.key, p.value));

        currentCapacity = newCapacity;
        hashTable = newHashTable.hashTable;
    }

    /**************PART 1.2*******************/

    public String[] mostFrequentStrings(String[] in)
    {
        HashTableChaining<String, Integer> accumulator = new HashTableChaining<>(11);

        Stream.of(in).forEach(s -> {
            int val = accumulator.getVal(s);
            accumulator.remove(s);
            accumulator.insert(s, val + 1);
        });

        int numberOfTopValues = 5;

        return accumulator.hashTable.stream()
                .flatMap(List::stream)
                .sorted((p1, p2) -> Integer.compare(p2.value, p1.value))
                .limit(numberOfTopValues)
                .map(p -> p.key)
                .collect(Collectors.toList())
                .toArray(new String[numberOfTopValues]);
    }

}
